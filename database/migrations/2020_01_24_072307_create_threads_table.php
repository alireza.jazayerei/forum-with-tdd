<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThreadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('threads', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('slug')->unique()->nullable();
            $table->unsignedBigInteger("best_reply_id")->nullable();
            $table->boolean("locked")->default(false);
            $table->unsignedBigInteger("replies_count")->default(0);
            $table->unsignedBigInteger("creator_id");
            $table->unsignedBigInteger("channel_id");
            $table->string('title');
            $table->string('text', 240);
            $table->timestamps();

            $table->foreign('creator_id')
                ->references('id')
                ->on("users")
                ->onDelete('cascade');

            $table->foreign('channel_id')
                ->references('id')
                ->on("channels")
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('threads');
    }
}
