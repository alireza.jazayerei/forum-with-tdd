<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Channel;
use App\Model;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(\App\Thread::class, function (Faker $faker) {
    $title = $faker->sentence(1);
    return [
        "creator_id" => function () {
            return factory(User::class)->create();
        },
        "channel_id" => function () {
            return factory(Channel::class)->create();
        },
        "title" => $title,
        "text" => $faker->text(240),
        "locked" => false,
    ];
});
