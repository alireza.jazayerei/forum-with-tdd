<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Reply;
use App\User;
use Faker\Generator as Faker;

$factory->define(Reply::class, function (Faker $faker) {
    return [
        "body" => $faker->sentence,
        "user_id" => function () {
            return factory(User::class)->create();
        },
        "thread_id" => function () {
            return factory(\App\Thread::class)->create();
        },
    ];
});
