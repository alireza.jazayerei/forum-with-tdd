<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/threads', "ThreadController@index")->name('threads.index');
Route::get('/threads/create', "ThreadController@create")->middleware('auth')->name('threads.create');
Route::post('/threads', "ThreadController@store")->middleware('auth')->name('threads.store');
Route::patch('/threads/{channel}/{thread}', "ThreadController@update")->middleware('auth')->name('threads.update');
Route::get('/threads/{channel}/{thread}', "ThreadController@show")->name('threads.show');
Route::delete('/threads/{channel}/{thread}', "ThreadController@destroy")->middleware('auth')->name('threads.destroy');

Route::post('/threads/{channel}/{thread}/reply', "ReplyThreadController@store")->middleware('auth')->name('reply.threads.store');
Route::delete("/replies/{reply}", "ReplyThreadController@destroy")->middleware('auth')->name('reply.destroy');
Route::patch("/replies/{reply}", "ReplyThreadController@update")->middleware('auth')->name('reply.update');

Route::get('/threads/{channel}', "channelController@index")->name("channels.index");

Route::post("reply/{reply}/favorites", "ReplyFavoritesController@store")->middleware('auth')->name('reply.favorites.store');
Route::delete("reply/{reply}/favorites", "ReplyFavoritesController@destroy")->middleware('auth')->name('reply.favorites.destroy');

Route::get('profiles/{user}', "ProfileController@show")->name('profiles.show');

Route::post('/threads/{channel}/{thread}/subscribe', "SubscriptionController@store")->middleware('auth')->name('subscription.store');
Route::delete('/threads/{channel}/{thread}/subscribe', "SubscriptionController@destroy")->middleware('auth')->name('subscription.destroy');

Route::delete("/notifications/{notification}", "NotificationController@destroy")->middleware('auth')->name('notifications.destroy');
Route::get("/notifications", "NotificationController@index")->middleware('auth')->name('notifications.index');

Route::get('users', "UserController@index")->name('users.index');

Route::post("/avatars", "AvatarController@store")->middleware('auth')->name('avatars.store');

Route::post("/reply/{reply}/best", "BestReplyController@store")->middleware('auth')->name('best.reply.store');

Route::post("/lock-threads/{thread}","LockThreadController@store")->middleware('auth')->name('lock-threads.store');
Route::delete("/lock-threads/{thread}","LockThreadController@destroy")->middleware('auth')->name('lock-threads.destroy');
