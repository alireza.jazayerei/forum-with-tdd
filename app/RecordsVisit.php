<?php


namespace App;


use Illuminate\Support\Facades\Redis;

trait RecordsVisit
{
    public function recordVisit()
    {
        Redis::incr($this->visitCacheKey());
    }

    public function visitCacheKey()
    {
        return class_basename($this) . $this->id . "visits";
    }

    public function resetVisits()
    {
        Redis::del($this->visitCacheKey());
    }

    public function visits()
    {
        return Redis::get($this->visitCacheKey());
    }
}
