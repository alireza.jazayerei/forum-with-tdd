<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = ["user_id", "subject_id", "subject_type", "type","created_at"];

    public static function feed($user, $take = 50)
    {
        return static::query()
            ->with("subject")
            ->latest()
            ->where('user_id', $user->id)
            ->get()
            ->groupBy(function ($activity) {
                return $activity->created_at->format('Y-m-d');
            });
    }

    public function subject()
    {
        return $this->morphTo();
    }
}
