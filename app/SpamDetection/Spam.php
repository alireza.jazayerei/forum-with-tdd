<?php


namespace App\SpamDetection;

use Exception;

class Spam
{
    protected $detections = [
        InvalidKeyWords::class,
        KeyHeldDown::class,
    ];

    public function detect($body)
    {
        foreach ($this->detections as $detection) {
            resolve($detection)->detect($body);
        }
        return false;
    }

}
