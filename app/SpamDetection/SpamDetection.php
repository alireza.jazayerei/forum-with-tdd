<?php


namespace App\SpamDetection;


interface SpamDetection
{
    public function detect($body);
}
