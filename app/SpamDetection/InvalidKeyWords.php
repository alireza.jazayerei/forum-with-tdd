<?php


namespace App\SpamDetection;


use Exception;

class InvalidKeyWords implements SpamDetection
{
    private $invalidKeys = [
        "spam",
    ];

    public function detect($body)
    {
        foreach ($this->invalidKeys as $key) {
            if (stripos($body, $key) !== false) {
                throw new Exception("spaaaammmm");
            }
        }
    }

}
