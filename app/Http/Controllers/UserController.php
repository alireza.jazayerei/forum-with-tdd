<?php

namespace App\Http\Controllers;

use App\User;

class UserController extends Controller
{
    public function index()
    {
        return User::where('name', 'LIKE', request()->get('name') . "%")
            ->pluck('name')
            ->take(5);
    }
}
