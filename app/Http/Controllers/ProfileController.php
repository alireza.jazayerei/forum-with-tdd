<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function show(User $user)
    {
        $user=$user->load("threads");
        return view('ProfilesShow', compact("user"));
    }
}
