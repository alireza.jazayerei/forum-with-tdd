<?php

namespace App\Http\Controllers;

use App\Thread;
use Illuminate\Http\Request;

class LockThreadController extends Controller
{
    public function store(Thread $thread)
    {
        $this->authorize("lock", $thread);

        $thread->update(["locked" => true]);
    }

    public function destroy(Thread $thread)
    {
        $this->authorize("lock", $thread);

        $thread->update(["locked" => false]);
    }
}
