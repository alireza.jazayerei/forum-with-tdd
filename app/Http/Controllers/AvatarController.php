<?php

namespace App\Http\Controllers;

use App\Http\Requests\AvatarRequest;
use App\User;
use Illuminate\Http\Request;

class AvatarController extends Controller
{
    public function store(AvatarRequest $request)
    {
        auth()->user()->update([
            "avatar_path" => $request->file("avatar")->store("avatars", "local"),
        ]);
    }
}
