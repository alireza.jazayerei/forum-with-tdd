<?php

namespace App\Http\Controllers;

use App\Channel;
use Illuminate\Http\Request;

class channelController extends Controller
{
    public function index(Channel $channel)
    {
        $threads = $channel->threads;
        return view('ChannelIndex', compact('threads'));
    }
}
