<?php

namespace App\Http\Controllers;

use App\Filters\ThreadFilter;
use App\Http\Requests\ThreadRequest;
use App\Thread;
use App\Trending;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class ThreadController extends Controller
{
    /**
     * @var Trending
     */
    public $trending;

    public function __construct(Trending $trending)
    {
        $this->trending = $trending;
    }

    public function index(ThreadFilter $filters)
    {
        $threads = Thread::query()->with(["channel", "replies"])->latest()->filter($filters)->get();
        if (request()->wantsJson()) {
            return $threads;
        }
        $trendings = $this->trending->get();
        return view('ThreadsIndex', compact('threads', "trendings"));
    }

    public function store(ThreadRequest $request)
    {
        $thread = auth()->user()->threads()->create([
            "title" => $request->input('title'),
            "text" => $request->input('text'),
            "channel_id" => $request->input('channel_id'),
        ]);

        if ($request->wantsJson()) {
            return $thread;
        }

        return redirect($thread->path());
    }

    public function update(ThreadRequest $request, $channelId, Thread $thread)
    {
        $this->authorize("update", $thread);
        $thread->update([
            "text" => $request->input('text'),
            "title" => $request->input('title'),
        ]);
    }

    public function show($channelId, Thread $thread)
    {
        if (auth()->check()) {
            cache::forever(auth()->user()->visitedThreadCacheKey($thread), Carbon::now());
        }

        $this->trending->push($thread);

        return view('ThreadsShow', compact('thread'));
    }

    public function destroy($channelId, Thread $thread)
    {
        $this->authorize("update", $thread);
        $thread->delete();
    }
}
