<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReplyRequest;
use App\Reply;
use App\Thread;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ReplyThreadController extends Controller
{
    public function store(ReplyRequest $request, $channelId, Thread $thread)
    {
        if (Gate::denies("create", new Reply())) {
            return response("sorry you are adding reply too often", 422);
        }
        if ($thread->locked) {
            return response("thread is locked", 422);
        }
        $thread->addReply([
            "body" => $request->input('body'),
            "user_id" => auth()->user()->id,
        ]);

        return redirect($thread->path());
    }

    public function update(Request $request, Reply $reply)
    {
        $this->authorize("update", $reply);
        $reply->update([
            "body" => $request->input('body'),
        ]);
        return back();
    }

    public function destroy(Reply $reply)
    {
        $this->authorize("update", $reply);
        $reply->delete();
        return back();
    }
}
