<?php

namespace App;

use App\Filters\QueryFilter;
use App\Providers\ReplyAdded;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class Thread extends Model
{
    use RecordsActivity, RecordsVisit;

    protected static $activityEvents = ["created"];
    protected $fillable = ["title", "text", "channel_id", "slug", "best_reply_id", "locked"];
    protected $appends = ["isSubscribed"];
    protected $casts = ["locked" => "boolean"];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($thread) {
            $thread->replies->each->delete();
        });

        static::created(function ($thread) {
            $thread->update(["slug" => $thread->title]);
        });
    }

    public function path()
    {
        return "threads/" . $this->channel->slug . "/" . $this->slug;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    public function addReply($reply)
    {
        $reply = $this->replies()->create($reply);

        event(new ReplyAdded($this, $reply));
    }

    public function replies()
    {
        return $this->hasMany(Reply::class)->withCount("favorites")->with("owner");
    }

    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    public function scopeFilter(Builder $query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }

    public function subscribe()
    {
        $this->subscriptions()->create([
            "user_id" => auth()->id(),
        ]);
    }

    public function subscriptions()
    {
        return $this->hasMany(ThreadSubscription::class);
    }

    public function unsubscribe()
    {
        $this->subscriptions()->where('user_id', auth()->id())->delete();
    }

    public function getIsSubscribedAttribute()
    {
        return $this->subscriptions()->where('user_id', auth()->id())->exists();
    }

    public function hasUpdates()
    {
        $key = auth()->user()->visitedThreadCacheKey($this);
        return $this->updated_at > cache::get($key);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function setSlugAttribute($value)
    {
        $slug = Str::slug($value);
        if (static::whereSlug($slug)->exists()) {
            $slug = $slug . "-" . $this->id;
        }
        $this->attributes['slug'] = $slug;
    }

    public function markBestReply(Reply $reply)
    {
        $this->update(["best_reply_id" => $reply->id]);
    }
}
