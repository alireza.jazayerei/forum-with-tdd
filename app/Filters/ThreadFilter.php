<?php


namespace App\Filters;


use App\User;

class ThreadFilter extends QueryFilter
{
    public function by($name)
    {
        $user = User::where("name", $name)->firstOrFail();
        return $this->builder->where("creator_id", $user->id);
    }

    public function popular()
    {
        $this->builder->getQuery()->orders = [];
        return $this->builder->orderBy("replies_count", "desc");
    }

    public function unanswered()
    {
        return $this->builder->where("replies_count", "0");
    }
}
