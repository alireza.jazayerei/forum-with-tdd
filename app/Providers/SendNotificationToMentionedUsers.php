<?php

namespace App\Providers;

use App\Notifications\UserMentioned;
use App\Providers\ReplyAdded;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendNotificationToMentionedUsers
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param ReplyAdded $event
     * @return void
     */
    public function handle(ReplyAdded $event)
    {
        User::whereIn("name", $event->reply->mentionedUsers())->get()
            ->each
            ->notify(new UserMentioned($event->reply));
    }
}
