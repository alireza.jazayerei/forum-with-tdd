<?php

namespace App\Providers;

use App\Providers\ReplyAdded;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendReplyAddedNotificationToThreadSubscribers
{
    /**
     * Handle the event.
     *
     * @param ReplyAdded $event
     * @return void
     */
    public function handle(ReplyAdded $event)
    {
        $event->thread->subscriptions
            ->where('user_id', "!=", $event->reply->user_id)
            ->each
            ->notify($event->reply);
    }
}
