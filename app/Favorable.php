<?php


namespace App;


trait Favorable
{
    public static function bootFavorable()
    {
        static::deleting(function ($model){
            $model->favorites->each->delete();
        });
    }

    public function favorite()
    {
        if (!$this->favorites()->where("user_id", auth()->id())->exists()) {
            $this->favorites()->create([
                "user_id" => auth()->id(),
            ]);
        }
    }

    public function favorites()
    {
        return $this->morphMany(Favorite::class, "favorable");
    }

    public function unfavorite()
    {
        $this->favorites()->where("user_id", auth()->id())->get()->each->delete();
    }
}
