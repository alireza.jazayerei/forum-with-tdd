<?php


namespace App;


trait RecordsActivity
{
    public static function bootRecordsActivity()
    {
        if (!auth()->check()) return;
        foreach (self::activityEvents() as $event) {
            static::$event(function ($model) use ($event) {
                $model->recordActivity($model->activityDescription($event));
            });
        }
        static::deleting(function ($model){
            $model->activity()->delete();
        });
    }

    private static function activityEvents()
    {
        if (isset(self::$activityEvents)) {
            return self::$activityEvents;
        }
        return ["created"];
    }

    private function recordActivity($description)
    {
        $this->activity()->create([
            "user_id" => auth()->id(),
            "type" => $description,
        ]);
    }

    public function activity()
    {
        return $this->morphMany(Activity::class, "subject");
    }

    private function activityDescription($event)
    {
        return $event . "_" . strtolower(class_basename($this));
    }
}
