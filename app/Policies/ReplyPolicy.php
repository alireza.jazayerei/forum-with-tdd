<?php

namespace App\Policies;

use App\Reply;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReplyPolicy
{
    use HandlesAuthorization;

    public function update(User $user, Reply $reply)
    {
        return $user->is($reply->owner);
    }

    public function create(User $user)
    {
        if (! $user->fresh()->lastReply) {
            return true;
        }
        return ! $user->lastReply->wasJustPublished();
    }
}
