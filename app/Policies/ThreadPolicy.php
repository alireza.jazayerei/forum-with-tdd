<?php

namespace App\Policies;

use App\Thread;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ThreadPolicy
{
    use HandlesAuthorization;

    public function update(User $user, Thread $thread)
    {
        return $user->is($thread->creator);
    }

    public function lock(User $user, Thread $thread)
    {
        return $user->isAdmin();
    }
}
