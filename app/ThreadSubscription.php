<?php

namespace App;

use App\Notifications\ReplyAddedNotification;
use Illuminate\Database\Eloquent\Model;

class ThreadSubscription extends Model
{
    protected $table = 'thread_subscription';
    protected $fillable = ["user_id", "thread_id"];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function thread()
    {
        return $this->belongsTo(Thread::class);
    }

    public function notify($reply)
    {
        return $this->user->notify(new ReplyAddedNotification($this->thread, $reply));
    }
}
