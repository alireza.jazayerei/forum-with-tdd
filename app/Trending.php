<?php


namespace App;


use Illuminate\Support\Facades\Redis;

class Trending
{
    public function get()
    {
        return array_map("json_decode", Redis::zrevrange(config("trending_threads"), "0", "4"));
    }

    public function push($thread)
    {
        Redis::zincrby(config("trending_threads"), 1, json_encode([
            "title" => $thread->title,
            "path" => $thread->path(),
        ]));
    }

    public function reset()
    {
        Redis::del(config("trending_threads"));
    }
}
