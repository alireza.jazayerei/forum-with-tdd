<?php

namespace Tests\Feature;

use App\Activity;
use App\Channel;
use App\Reply;
use App\Thread;
use Facades\Tests\Arranges\ThreadFactory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ManageThreadTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function guests_cannot_create_threads()
    {
        $this->get('/threads/create')->assertRedirect("/login");
        $this->post('/threads', [])->assertRedirect("/login");
    }

    /** @test */
    public function an_authenticated_user_can_create_thread()
    {
        $this->withoutExceptionHandling();
        $user = $this->signIn();
        $thread = factory(Thread::class)->raw();
        $this->followingRedirects()->post('/threads', $thread)
            ->assertSee($thread["title"])
            ->assertSee($thread["text"])
            ->assertSee($user->name);
    }

    /** @test */
    public function thread_requires_unique_slug()
    {
        $this->signIn();

        factory(Thread::class, 3)->create();

        $threadWithSameTitle = factory(Thread::class)->raw(["title" => "foo-title-25"]);

        $this->postJson("/threads", $threadWithSameTitle)->json();

        $this->assertTrue(Thread::whereSlug("foo-title-25")->exists());

        $thread = $this->postJson("/threads", $threadWithSameTitle)->json();

        $this->assertTrue(Thread::whereSlug("foo-title-25-{$thread["id"]}")->exists());
    }

    /** @test */
    public function title_is_required()
    {
        $this->signIn();
        $thread = factory(Thread::class)->raw(["title" => ""]);
        $this->post("/threads", $thread)
            ->assertSessionHasErrors(["title"]);
    }

    /** @test */
    public function text_is_required()
    {
        $this->signIn();
        $thread = factory(Thread::class)->raw(["text" => ""]);
        $this->post("/threads", $thread)
            ->assertSessionHasErrors(["text"]);
    }

    /** @test */
    public function channel_is_required()
    {
        $this->signIn();
        $thread = factory(Thread::class)->raw(["channel_id" => ""]);
        $this->post("/threads", $thread)
            ->assertSessionHasErrors(["channel_id"]);
    }

    /** @test */
    public function channel_should_be_valid()
    {
        $this->signIn();
        factory(Channel::class, 3)->create();
        $thread = factory(Thread::class)->raw(["channel_id" => "9999"]);
        $this->post("/threads", $thread)
            ->assertSessionHasErrors(["channel_id"]);
    }

    /** @test */
    public function authorized_user_can_delete_threads()
    {
        $this->signIn();

        $thread = factory(Thread::class)->create(["creator_id" => auth()->id()]);
        $reply = factory(Reply::class)->create([
            "thread_id" => $thread->id,
            "user_id" => auth()->id(),
        ]);
        $replyId = $thread->replies()->first()->id;

        $this->delete($thread->path());
        $this->assertDatabaseMissing("replies", ["id" => $replyId]);
        $this->assertDatabaseMissing("threads", ["id" => $thread->id]);

        $this->assertDatabaseMissing("activities", [
            "user_id" => $thread->creator->id,
            "subject_id" => $thread->id,
            "subject_type" => "App\Thread",
        ]);

        $this->assertDatabaseMissing("activities", [
            "user_id" => $reply->owner->id,
            "subject_id" => $reply->id,
            "subject_type" => "App\Reply",
        ]);

        $this->assertCount(0, Activity::all());
    }

    /** @test */
    public function unauthorized_users_cannot_delete_threads()
    {
        $thread = ThreadFactory::withReply(1)->create();
        $this->delete($thread->path())->assertRedirect("/login");
        $this->signIn();
        $this->delete($thread->path())->assertStatus(403);
    }

    /** @test */
    public function authorized_users_can_update_threads()
    {
        $this->signIn();
        $thread = factory(Thread::class)->create(["creator_id" => auth()->id()]);

        $this->patch($thread->path(), [
            "text" => "Im changed",
            "title" => "changed too ;)",
            "channel_id" => $thread->channel->id,
        ]);

        $this->assertEquals("Im changed", $thread->fresh()->text);
        $this->assertEquals("changed too ;)", $thread->fresh()->title);
    }

    /** @test */
    public function text_and_title_are_required_to_update_a_thread()
    {
        $this->signIn();
        $thread = factory(Thread::class)->create(["creator_id" => auth()->id()]);

        $this->patch($thread->path(), [])
            ->assertSessionHasErrors(["text", "title"]);
    }

    /** @test */
    public function unauthorized_users_cannot_update_threads()
    {
        $this->signIn();
        $thread = factory(Thread::class)->create();

        $this->patch($thread->path(), [
            "text" => "Im changed",
            "title" => "changed too ;)",
            "channel_id" => $thread->channel->id,
        ])
            ->assertStatus(403);
    }
}
