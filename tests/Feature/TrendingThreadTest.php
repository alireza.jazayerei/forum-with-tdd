<?php

namespace Tests\Feature;

use App\Thread;
use App\Trending;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Redis;
use Tests\Arranges\FakeTrending;
use Tests\TestCase;

class TrendingThreadTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        Parent::setUp();
        $this->trending = new Trending();
        $this->trending->reset();
    }

    /** @test */
    public function it_increments_threads_score_after_each_visit()
    {
        app()->instance(Trending::class, new FakeTrending());
        $trending = app(Trending::class);

        $trending->assertEmpty();

        $thread = factory(Thread::class)->create();
        $this->get($thread->path());

        $trending->assertCount(1);
        $this->assertEquals($thread->title, ($trending->threads[0])->title);
    }
}
