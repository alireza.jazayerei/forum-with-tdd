<?php

namespace Tests\Feature;

use App\Reply;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FavoritesTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function guests_cannot_favorite_a_reply()
    {
        $reply = factory(Reply::class)->create();
        $this->post('/reply/' . $reply->id . '/favorites')
            ->assertRedirect("/login");
    }

    /** @test */
    public function replies_can_be_favored()
    {
        $this->signIn();
        $reply = factory(Reply::class)->create();
        $this->post('/reply/' . $reply->id . '/favorites');
        $this->assertCount(1, $reply->favorites);
    }

    /** @test */
    public function a_user_cannot_favorite_a_reply_multiple_times()
    {
        $this->signIn();
        $reply = factory(Reply::class)->create();
        $this->post('/reply/' . $reply->id . '/favorites');
        $this->post('/reply/' . $reply->id . '/favorites');
        $this->assertCount(1, $reply->favorites);
    }

    /** @test */
    public function a_user_can_unfavorite_a_reply()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $reply = factory(Reply::class)->create();
        $this->post('/reply/' . $reply->id . '/favorites');
        $this->assertCount(1, $reply->favorites);
        $this->delete('/reply/' . $reply->id . '/favorites');
        $this->assertCount(0, $reply->fresh()->favorites);
    }
}
