<?php

namespace Tests\Feature;

use App\Activity;
use App\Reply;
use App\Thread;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ActivityTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function creating_a_thread_generates_an_activity()
    {
        $user = $this->signIn();
        $thread = factory(Thread::class)->create(["creator_id" => $user->id]);
        $this->assertDatabaseHas("activities", [
            "user_id" => $user->id,
            "type" => "created_thread",
            "subject_id" => $thread->id,
            "subject_type" => "App\Thread"
        ]);
        $activity = Activity::first();
        $this->assertEquals($thread->id, $activity->subject->id);
    }

    /** @test */
    public function replying_a_thread_generates_an_activity()
    {
        $user = $this->signIn();
        factory(Reply::class)->create(["user_id" => $user->id]);
        $this->assertCount(2, Activity::all());
    }

    /** @test */
    public function it_can_fetch_a_users_feed()
    {
        $user = $this->signIn();
        factory(Thread::class, 2)->create(["creator_id" => $user->id]);
        $user->activity()->first()->update(['created_at' => Carbon::now()->subWeek()]);

        $feed = Activity::feed($user);

        $this->assertTrue($feed->keys()->contains(
            Carbon::now()->format('Y-m-d')
        ));
        $this->assertTrue($feed->keys()->contains(
            Carbon::now()->subWeek()->format('Y-m-d')
        ));
    }
}
