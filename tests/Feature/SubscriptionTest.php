<?php

namespace Tests\Feature;

use App\Thread;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SubscriptionTest extends TestCase
{
    use refreshDatabase;

    /** @test */
    public function an_authenticated_user_can_subscribe_to_a_thread()
    {
        $this->signIn();
        $thread=factory(Thread::class)->create();
        $this->post($thread->path().'/subscribe');
        $this->assertCount(1,$thread->subscriptions);
    }

    /** @test */
    public function an_authenticated_user_can_unubscribe_a_thread()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $thread=factory(Thread::class)->create();
        $this->post($thread->path().'/subscribe');
        $this->assertCount(1,$thread->subscriptions);
        $this->delete($thread->path().'/subscribe');
        $this->assertCount(0,$thread->fresh()->subscriptions);
    }
}
