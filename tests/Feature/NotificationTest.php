<?php

namespace Tests\Feature;

use App\Notifications\ReplyAddedNotification;
use App\Thread;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class NotificationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_gets_notified_when_a_reply_is_added_to_his_subscribed_thread()
    {
        $user = $this->signIn();
        $thread = factory(Thread::class)->create();
        $thread->subscribe();
        $this->assertCount(0, $user->notifications);
        $thread->addReply([
            "user_id" => factory(User::class)->create()->id,
            "thread_id" => $thread->id,
            "body" => "hooora",
        ]);
        $this->assertCount(1, $user->fresh()->notifications);
    }

    /** @test */
    public function a_user_wont_get_notification_for_his_reply()
    {
        $user = $this->signIn();
        $thread = factory(Thread::class)->create();
        $thread->subscribe();
        $this->assertCount(0, $user->notifications);
        $thread->addReply([
            "user_id" => auth()->id(),
            "thread_id" => $thread->id,
            "body" => "hoooriooo",
        ]);
        $this->assertCount(0, $user->fresh()->notifications);
    }

    /** @test */
    public function user_can_mark_notifications_as_read()
    {
        $user = $this->signIn();
        factory(DatabaseNotification::class)->create();
        $this->assertCount(1, $user->fresh()->unreadNotifications);
        $this->delete("/notifications/" . $user->notifications()->first()->id);
        $this->assertCount(0, $user->fresh()->unreadNotifications);
    }

    /** @test */
    public function User_can_fetch_all_unread_notifications()
    {
        $this->signIn();
        factory(DatabaseNotification::class)->create();
        $response = $this->getJson('/notifications')->json();
        $this->assertCount(1, $response);
    }

    /** @test */
    public function notification_is_sent_to_thread_subscribers_when_reply_is_added()
    {
        Notification::fake();
        $user = $this->signIn();
        $thread = factory(Thread::class)->create();
        $thread->subscribe();
        $thread->addReply([
            "user_id" => factory(User::class)->create()->id,
            "thread_id" => $thread->id,
            "body" => "hooora",
        ]);
        Notification::assertSentTo($user, ReplyAddedNotification::class);
    }
}
