<?php

namespace Tests\Feature;

use App\Reply;
use App\Thread;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LockThreadTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function users_cannot_add_reply_in_locked_threads()
    {
        $this->signIn();

        $thread = factory(Thread::class)->create(["locked" => true]);

        $this->post($thread->path() . "/reply", ["body" => "jri"])->assertStatus(422);
    }

    /** @test */
    public function non_admin_users_cannot_lock_threads()
    {
        $this->signIn();
        $thread = factory(Thread::class)->create();

        $this->post('/lock-threads/' . $thread->slug)->assertStatus(403);
        $this->assertFalse($thread->fresh()->locked);
    }

    /** @test */
    public function administrator_can_lock_a_thread()
    {
        $this->signIn(factory(User::class)->state("admin")->create());
        $thread = factory(Thread::class)->create();

        $this->post("/lock-threads/" . $thread->slug);

        $this->assertTrue($thread->fresh()->locked);
    }

    /** @test */
    public function administrator_can_unlock_a_thread()
    {
        $this->signIn(factory(User::class)->state("admin")->create());
        $thread = factory(Thread::class)->create(["locked" =>true]);

        $this->delete("/lock-threads/" . $thread->slug);

        $this->assertFalse($thread->fresh()->locked);
    }
}
