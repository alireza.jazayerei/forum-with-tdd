<?php

namespace Tests\Feature;

use App\Reply;
use App\Thread;
use Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Arranges\ThreadFactory;
use Tests\TestCase;

class ParticipateInForumTest extends TestCase
{
    use refreshDatabase;

    /** @test */
    public function unauthenticated_users_cannot_add_reply()
    {
        $thread = factory(Thread::class)->create();
        $this->post($thread->path() . "/reply", [])->assertRedirect("/login");
    }

    /** @test */
    public function an_authenticated_user_can_participate_in_forum()
    {
        $this->withoutExceptionHandling();
        $thread = factory(Thread::class)->create();
        $user = $this->signIn();
        $reply = factory(Reply::class)->make(["user_id" => $user->id]);

        $this->followingRedirects()->post($thread->path() . "/reply", $reply->toArray())->assertSee($reply->body);
        $this->assertEquals(1, $thread->fresh()->replies_count);
    }

    /** @test */
    public function body_is_required_to_make_a_reply()
    {
        $thread = factory(Thread::class)->create();
        $this->signIn();
        $reply = factory(Reply::class)->raw(["body" => ""]);
        $this->post($thread->path() . "/reply", $reply)->assertSessionHasErrors(["body"]);
    }

    /** @test */
    public function unauthorized_users_cannot_delete_replies()
    {
        $reply = factory(Reply::class)->create();
        $this->delete("/replies/" . $reply->id)->assertRedirect("/login");
        $this->signIn();
        $this->delete("/replies/" . $reply->id)->assertStatus(403);
        $this->assertDatabaseHas("replies", ["id" => $reply->id]);
    }

    /** @test */
    public function authorized_users_may_delete_reply()
    {
        $user = $this->signIn();
        $reply = factory(Reply::class)->create(["user_id" => $user->id]);
        $this->delete("/replies/" . $reply->id);
        $this->assertDatabaseMissing("replies", ["id" => $reply->id]);
        $this->assertEquals(0, $reply->thread->fresh()->replies_count);
    }

    /** @test */
    public function authorized_users_may_edit_replies()
    {
        $this->signIn();
        $reply = factory(Reply::class)->create(["user_id" => auth()->id()]);
        $this->patch("/replies/" . $reply->id, ["body" => $changedBody = "Im changed you fool"]);
        $this->assertDatabaseHas("replies", [
            "id" => $reply->id,
            "body" => $changedBody,
        ]);
    }

    /** @test */
    public function unauthorized_users_cannot_edit_replies()
    {
        $reply = factory(Reply::class)->create();
        $this->patch("/replies/" . $reply->id, ["body" => "Im changed you fool"])->assertRedirect("/login");
        $this->signIn();
        $this->patch("/replies/" . $reply->id, ["body" => "Im changed you fool"])->assertStatus(403);
    }

    /** @test */
    public function a_reply_containing_spam_wont_add()
    {
        $this->signIn();
        $thread = factory(Thread::class)->create();
        $this->post($thread->path() . "/reply", ["body" => "zzzzzzz",])
        ->assertSessionHasErrors(["body" =>"The body contains spam"]);

    }

    /** @test */
    public function users_cannot_add_more_than_one_reply_per_minute()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $thread = factory(Thread::class)->create();
        $this->post($thread->path() . "/reply", ["body" =>"toho body"])
            ->assertStatus(302);
        $this->post($thread->path() . "/reply", ["body" =>"toho johugy body"])
            ->assertStatus(422);
    }
}
