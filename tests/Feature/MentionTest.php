<?php

namespace Tests\Feature;

use App\Thread;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MentionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function mentioned_users_in_reply_get_notified()
    {
        $this->signIn();
        $jane = factory(User::class)->create(["name" => "JaneDoe"]);
        $thread = factory(Thread::class)->create();
        $this->post($thread->path() . "/reply", ["body" => "hello @JaneDoe how are you?"]);
        $this->assertCount(1, $jane->notifications);
    }

    /** @test */
    public function users_get_suggestion_while_mentioning_others()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        factory(User::class)->create(["name" => "JohnDoe"]);
        factory(User::class)->create(["name" => "JoshDoe"]);
        factory(User::class)->create(["name" => "MaryPoppins"]);

        $response = $this->get('/users?name=Jo')->json();
        $this->assertEquals($response, ["JohnDoe", "JoshDoe"]);
    }
}
