<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class AvatarTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function only_members_can_add_avatar()
    {
        $this->post("/avatars")->assertRedirect("/login");
    }

    /** @test */
    public function avatar_must_be_valid()
    {
        $this->signIn();
        $this->post("/avatars", ["avatar" => "im invalid :)"])->assertSessionHasErrors(["avatar"]);
    }

    /** @test */
    public function users_can_upload_avatar()
    {
        $this->signIn();
        $storage = Storage::fake("local");
        $this->post("/avatars", ["avatar" => $avatar = UploadedFile::fake()->image("avatar.jpg")]);
        $this->assertEquals("avatars/" . $avatar->hashName(), auth()->user()->avatar_path);
        $storage->assertExists("avatars/" . $avatar->hashName());
    }
}
