<?php

namespace Tests\Feature;

use App\Thread;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProfileTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_have_a_profile()
    {
        $user =  factory(User::class)->create();
        $this->get('/profiles/' . $user->name)
            ->assertSee($user->name);
    }

    /** @test */
    public function a_profile_contains_users_threads()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(["name" =>"ali"]);
        $thread = factory(Thread::class)->create(["creator_id" => $user->id]);
        $this->get('/profiles/' . $user->name)
            ->assertSee($thread->title);
    }
}
