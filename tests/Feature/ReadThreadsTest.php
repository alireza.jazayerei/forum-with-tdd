<?php

namespace Tests\Feature;

use App\Channel;
use App\Reply;
use App\Thread;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Facades\Tests\Arranges\ThreadFactory;
use Tests\TestCase;

class ReadThreadsTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function user_can_see_all_threads()
    {
        $thread = factory(Thread::class)->create();
        $this->get('/threads')
            ->assertStatus(200)
            ->assertSee($thread->title);
    }

    /** @test */
    public function user_can_view_a_thread()
    {
        $thread = factory(Thread::class)->create();
        $this->get($thread->path())
            ->assertStatus(200)
            ->assertSee($thread->title)
            ->assertSee($thread->text);
    }

    /** @test */
    public function users_can_see_threads_replies()
    {
        $this->withoutExceptionHandling();
        $thread = ThreadFactory::withReply(1)->create();

        $this->get($thread->path())
            ->assertSee($thread->replies->first()->body)
            ->assertSee($thread->replies->first()->body);
    }

    /** @test */
    public function a_user_can_see_a_channels_threads()
    {
        $channel = factory(Channel::class)->create();
        $threadInChannel = factory(Thread::class)->create(["channel_id" => $channel->id]);
        $threadNotInChannel = factory(Thread::class)->create();
        $this->get('/threads/' . $channel->slug)
            ->assertSee($threadInChannel->title)
            ->assertDontSee($threadNotInChannel);
    }

    /** @test */
    public function a_user_can_filter_threads_by_username()
    {
        $user = $this->signIn(factory(User::class)->create(["name" => "alireza"]));
        $threadForAlireza = factory(Thread::class)->create(["creator_id" => $user->id]);
        $threadNotForAlireza = factory(Thread::class)->create();
        $this->get('/threads?by=' . $user->name)
            ->assertSee($threadForAlireza->title)
            ->assertDontSee($threadNotForAlireza->title);
    }

    /** @test */
    public function a_user_can_filter_threads_by_popularity()
    {
        factory(Thread::class)->create();
        ThreadFactory::withReply(2)->create();
        ThreadFactory::withReply(3)->create();

        $response = $this->getJson("/threads?popular")->json();

        $this->assertEquals(["3", "2", "0"], array_column($response, "replies_count"));
    }

    /** @test */
    public function a_user_can_filter_threads_by_unanswered()
    {
        $threadWithNoReply = factory(Thread::class)->create();
        $threadWithReply = (factory(Reply::class)->create())->thread;

        $this->get('/threads?unanswered=1')
            ->assertSee($threadWithNoReply->title)
            ->assertDontSee($threadWithReply->title);
    }

}
