<?php

namespace Tests\Feature;

use Facades\Tests\Arranges\ThreadFactory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class BestReplyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function thread_creator_may_mark_a_reply_as_best()
    {
        $thread = ThreadFactory::withReply(2)->create();
        $this->signIn($thread->creator);
        $this->assertFalse($thread->replies[1]->isBest());
        $this->post("/reply/" . $thread->replies[1]->id . "/best");
        $this->assertTrue($thread->fresh()->replies[1]->isBest());
    }

    /** @test */
    public function only_thread_owner_can_mark_reply_as_best()
    {
        $thread = ThreadFactory::withReply(2)->create();
        $this->signIn();
        $this->post("/reply/" . $thread->replies[1]->id . "/best")
            ->assertStatus(403);
        $this->assertFalse($thread->replies[1]->isBest());
    }

    /** @test */
    public function when_a_best_reply_is_deleted_threads_best_reply_id_is_updated()
    {
        $this->withoutExceptionHandling();
        $thread = ThreadFactory::withReply(1)->create();
        $this->signIn($thread->replies->first()->owner);
        $thread->markBestReply($thread->replies->first());
        $this->delete("/replies/" . $thread->replies->first()->id);
        $this->assertDatabaseMissing("replies",["id" => $thread->replies->first()->id]);
        $this->assertNull($thread->fresh()->best_reply_id);
    }
}
