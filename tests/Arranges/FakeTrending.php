<?php


namespace Tests\Arranges;


use App\Trending;
use PHPUnit\Framework\Assert;

class FakeTrending extends Trending
{
    public $threads = [];

    public function push($thread)
    {
        $this->threads[] = $thread;
    }

    public function assertEmpty()
    {
        Assert::assertEmpty($this->threads);
    }

    public function assertCount($count)
    {
        Assert::assertCount($count,$this->threads);
    }
}
