<?php

namespace Tests\Arranges;

use App\Reply;
use App\Thread;
use App\User;

class ThreadFactory
{
    private $replyCount = 0;

    public function create()
    {
        $thread = factory(Thread::class)->create([
            "creator_id" => factory(User::class),
        ]);

        factory(Reply::class, $this->replyCount)->create([
            "thread_id" => $thread->id,
        ]);

        return $thread;
    }

    public function withReply($replyCount = 0)
    {
        $this->replyCount = $replyCount;
        return $this;
    }
}
