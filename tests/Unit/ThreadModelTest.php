<?php

namespace Tests\Unit;

use App\Channel;
use App\Reply;
use App\Thread;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Facades\Tests\Arranges\ThreadFactory;
use Tests\TestCase;

class ThreadModelTest extends TestCase
{
    use refreshDatabase;

    /** @test */
    public function it_has_creator()
    {
        $thread = factory(Thread::class)->create();
        $this->assertInstanceOf(User::class, $thread->creator);
    }

    /** @test */
    public function it_has_replies()
    {
        $thread = factory(Thread::class)->create();
        $this->assertInstanceOf(Collection::class, $thread->replies);
    }

    /** @test */
    public function it_can_add_reply()
    {
        $thread = factory(Thread::class)->create();
        $thread->addReply([
            "body" => "testingggg",
            "user_id" => factory(User::class)->create()->id,
        ]);
        $this->assertCount(1, $thread->replies);
    }

    /** @test */
    public function it_belongs_to_a_channel()
    {
        $thread = factory(Thread::class)->create();
        $this->assertInstanceOf(Channel::class, $thread->channel);
    }

    /** @test */
    public function it_has_a_path()
    {
        $thread = factory(Thread::class)->create();
        $this->assertEquals("threads/{$thread->channel->slug}/{$thread->slug}", $thread->path());
    }

    /** @test */
    public function users_can_subscribe_to_it()
    {
        $this->signIn();
        $thread = factory(Thread::class)->create();
        $thread->subscribe();
        $this->assertCount(1, $thread->subscriptions()->where('user_id', auth()->id())->get());
    }

    /** @test */
    public function users_can_unsubscribe_it()
    {
        $this->signIn();
        $thread = factory(Thread::class)->create();
        $thread->subscribe();
        $this->assertCount(1, $thread->subscriptions()->where('user_id', auth()->id())->get());
        $thread->unsubscribe();
        $this->assertCount(0, $thread->subscriptions()->where('user_id', auth()->id())->get());
    }

    /** @test */
    public function it_can_check_if_user_is_subscribed_to_it()
    {
        $this->signIn();
        $thread = factory(Thread::class)->create();
        $this->assertFalse($thread->isSubscribed);
        $thread->subscribe();
        $this->assertTrue($thread->isSubscribed);
    }

    /** @test */
    public function it_can_check_if_there_is_updates_for_user()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $thread = factory(Thread::class)->create();
        $this->assertTrue($thread->hasUpdates());
        $this->get($thread->path());
        $this->assertFalse($thread->hasUpdates());
    }

    /** @test */
    public function it_records_visits_for_each_thread()
    {
        $thread = factory(Thread::class)->create();

        $thread->resetVisits();

        $this->assertEquals(0, $thread->visits());

        $thread->recordVisit();

        $this->assertEquals(1, $thread->visits());

        $thread->recordVisit();

        $this->assertEquals(2, $thread->visits());
    }

}
