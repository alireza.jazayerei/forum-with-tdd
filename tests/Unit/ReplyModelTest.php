<?php

namespace Tests\Unit;

use App\Reply;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ReplyModelTest extends TestCase
{
    use refreshDatabase;

    /** @test */
    public function it_has_owner()
    {
        $reply = factory(Reply::class)->create();
        $this->assertInstanceOf(User::class, $reply->owner);
    }

    /** @test */
    public function it_can_be_favored()
    {
        $reply = factory(Reply::class)->create();
        $this->assertInstanceOf(Collection::class, $reply->favorites);
    }

    /** @test */
    public function it_knows_if_it_is_just_published()
    {
        $reply = factory(Reply::class)->create();
        $this->assertTrue($reply->wasJustPublished());
        $reply->created_at = Carbon::now()->subWeek();
        $this->assertFalse($reply->wasJustPublished());
    }

    /** @test */
    public function it_knows_the_mentioned_users_in_its_body()
    {
        $reply = factory(Reply::class)->create(["body" => "hi @JaneDoe and hi @JohnDoe"]);
        $this->assertEquals(["JaneDoe", "JohnDoe"], $reply->mentionedUsers());
    }

    /** @test */
    public function it_wraps_mentioned_users_in_anchor_tag()
    {
        $reply = factory(Reply::class)->create(["body" => "hi @JaneDoe and hi @JohnDoe"]);
        $this->assertEquals(
            'hi <a href="/profiles/JaneDoe">@JaneDoe</a> and hi <a href="/profiles/JohnDoe">@JohnDoe</a>',
            $reply->body
        );
    }

    /** @test */
    public function it_knows_if_its_the_best_reply()
    {
        $reply = factory(Reply::class)->create();
        $this->assertFalse($reply->isBest());
        $reply->thread->markBestReply($reply);
        $this->assertTrue($reply->isBest());
    }
}
