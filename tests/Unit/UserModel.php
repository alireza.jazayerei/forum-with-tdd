<?php

namespace Tests\Unit;

use App\Reply;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserModel extends TestCase
{
    use refreshDatabase;

    /** @test */
    public function it_has_threads()
    {
        $user = factory(User::class)->create();
        $this->assertInstanceOf(Collection::class, $user->threads);
    }

    /** @test */
    public function it_has_activity()
    {
        $user = factory(User::class)->create();
        $this->assertInstanceOf(Collection::class, $user->activity);
    }

    /** @test */
    public function it_fetches_users_last_reply()
    {
        $this->signIn();
        $reply = factory(Reply::class)->create(["user_id" => auth()->id()]);
        $this->assertEquals($reply->id, auth()->user()->lastReply->id);
    }

    /** @test */
    public function it_can_fetch_its_avatar_path()
    {
        $user = factory(User::class)->create();
        $this->assertEquals($user->avatar(), "default.jpg");
        $user->avatar_path = 'me.jpg';
        $this->assertEquals($user->avatar(), "me.jpg");
    }
}
