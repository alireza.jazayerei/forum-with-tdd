<?php

namespace Tests\Unit;

use App\Thread;
use App\Trending;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TrendingTest extends TestCase
{
    use RefreshDatabase;

    public function setUp() : void
    {
        Parent::setUp();

        $this->trending = new Trending();
        $this->trending->reset();
    }

    /** @test */
    public function trending_class_functionality()
    {
        $this->assertEmpty($this->trending->get());

        $thread1 = factory(Thread::class)->create();
        $thread2 = factory(Thread::class)->create();
        $this->get($thread1->path());

        $this->assertCount(1,$this->trending->get());

        $this->get($thread2->path());
        $this->get($thread2->path());
        $this->get($thread2->path());

        $this->assertEquals([$thread2->title,$thread1->title],array_column($this->trending->get(),"title"));

    }
}
