<?php

namespace Tests\Unit;

use App\Channel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ChannelModel extends TestCase
{
    use refreshDatabase;

    /** @test */
    public function it_consists_of_channels()
    {
        $channel = factory(Channel::class)->create();
        $this->assertInstanceOf(Collection::class, $channel->threads);
    }
}
