<?php

namespace Tests\Unit;

use App\SpamDetection\Spam;
use Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SpamTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_detects_invalid_keywords()
    {
        $this->withoutExceptionHandling();
        $spam = new Spam();
        $this->assertFalse($spam->detect("im so good"));
        $this->expectException(Exception::class);
        $spam->detect("im spam");
    }

    /** @test */
    public function it_detects_key_held_down()
    {
        $this->withoutExceptionHandling();
        $spam = new Spam();
        $this->expectException(Exception::class);
        $spam->detect("zzzzzzzz");
    }

}
